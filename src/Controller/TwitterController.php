<?php

namespace Drupal\my_form_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_form_demo\Controller\TwitterController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class TwitterController.
 *
 * @package Drupal\my_form_demo\Controller
 */
class TwitterController extends ControllerBase {

  /**
   * Twitter Handle.
   *
   * @return array
   *   Render array containing our message.
   */
  public function twitterHandle() {

    $config = $this->config('my_form_demo.twitter');

    return [
      '#markup' => $this->t('Our Twitter handle is %twitter_handle', [
        '%twitter_handle' => $config->get('twitter_handle', 'not set yet'),
      ]),
    ];
  }

}
