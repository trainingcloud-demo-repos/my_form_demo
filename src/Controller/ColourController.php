<?php

namespace Drupal\my_form_demo\Controller;

/**
 * @file
 * Contains \Drupal\my_form_demo\Controller\ColourController.
 */

use Drupal\Core\Controller\ControllerBase;

/**
 * Class ColourController.
 *
 * @package Drupal\my_form_demo\Controller
 */
class ColourController extends ControllerBase {

  /**
   * Favourite Colour.
   *
   * @return array
   *   Render array containing our message.
   */
  public function favouriteColour($fav) {

    return [
      '#markup' => $this->t('Your favourite colour is: %fav', [
        '%fav' => $fav,
      ]),
    ];
  }

}
