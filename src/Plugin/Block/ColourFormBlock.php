<?php

namespace Drupal\my_form_demo\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * A block that displays the ColourForm.
 *
 * @Block(
 *   id = "colour_form_block",
 *   admin_label = @Translation("Colour Form Block"),
 * )
 */
class ColourFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $form = \Drupal::formBuilder()->getForm('Drupal\my_form_demo\Form\ColourForm');

    return $form;
  }

}
