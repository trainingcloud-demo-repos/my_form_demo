<?php

namespace Drupal\my_form_demo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CircleForm.
 *
 * @package Drupal\my_form_demo\Form
 */
class CircleForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'circle_form';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['calculation'] = [
      '#type' => 'radios',
      '#title' => $this->t('Calculation'),
      '#options' => [
        'circumference' => $this->t('Circumference'),
        'area' => $this->t('Area'),
      ],
      '#description' => $this->t('Please choose whether to calculate the circumference or area of a circle'),
      '#required' => TRUE,
    ];

    $form['radius'] = [
      '#type' => 'number',
      '#title' => $this->t('Radius (m)'),
      '#description' => $this->t('Enter the radius of the circle in metres. This must be a number greater than or equal to zero.'),
      '#required' => TRUE,
    ];

    // Submit handlers should be grouped in an actions element with a key of
    // "actions" so that it gets styled correctly, and so that other modules may
    // add actions to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);

    $radius = $form_state->getValue('radius');

    // Validate the value of radius before we try to use it.
    if ($radius < 0) {
      $form_state->setErrorByName('radius', $this->t('Radius must be a number greater than or equal to zero.'));
    }
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $messenger = \Drupal::messenger();
    $radius = $form_state->getValue('radius');

    switch ($form_state->getValue('calculation')) {
      case 'circumference':
        $messenger->addMessage($this->t('The circumference of a circle with radius @radiusm is @circumferencem', [
          '@radius' => $radius,
          '@circumference' => 2 * pi() * $radius,
        ]));
        break;

      case 'area':
        $messenger->addMessage($this->t('The area of a circle with radius @radiusm is @aream<sup>2</sup>', [
          '@radius' => $radius,
          '@area' => pi() * pow($radius, 2),
        ]));
        break;
    }
  }

}
