<?php

namespace Drupal\my_form_demo\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class ColourForm extends FormBase {

  // Return unique string that identifies the form.
  public function getFormId() {

    return 'my_colour_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {

    // Add field.
    $form['fav_colour'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your favourite colour'),
      '#description' => $this->t('Allowed colours: red, green, or blue.')
    ];

    // Add submit button.
    $form['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the submitted value.
    $submitted_colour = $form_state->getValue('fav_colour');
    $allowed_colours = ['red', 'green', 'blue'];
    if (!in_array($submitted_colour, $allowed_colours)) {
        $form_state->setErrorByName(
          'fav_colour',
          $this->t("Colour must be one of the allowed colours.")
        );
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the submitted value.
    $submitted_colour = $form_state->getValue('fav_colour');
    
    // Redirect to our own 'yourfavcolour' route, passing along the submitted value as a parameter.
    // If, for example, 'red' was submitted, the user will be redirected to /colour/red.
    $form_state->setRedirect('my_form_demo.yourfavcolour', ['fav' => $submitted_colour]);
    return;
  }

}
