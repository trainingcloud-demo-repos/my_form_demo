<?php

namespace Drupal\my_form_demo\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class TwitterForm.
 *
 * @package Drupal\my_form_demo\Form
 */
class TwitterForm extends ConfigFormBase {

  /**
   * The name of the config object we'll be using.
   *
   * @var string
   *   This is in a variable as it's used in more than one place.
   */
  protected $configName = 'my_form_demo.twitter';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twitter_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      $this->configName,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    parent::buildForm($form, $form_state);

    $config = $this->config($this->configName);

    $form['twitter_handle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter Handle'),
      '#default_value' => $config->get('twitter_handle', ''),
      '#description' => $this->t("Please enter the twitter handle. It must start with '@' followed by one to fifteen alphanumeric characters or underscores."),
      '#required' => TRUE,
    ];

    // Submit handlers should be grouped in an actions element with a key of
    // "actions" so that it gets styled correctly, and so that other modules may
    // add actions to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Submit'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);

    $twitter_handle = $form_state->getValue('twitter_handle');

    if (!preg_match('#^@[a-zA-Z0-9_]{0,15}$#', $twitter_handle)) {
      $error_message = $this->t("Twitter handles must start with '@' followed by one to fifteen alphanumeric characters or underscores");
      $form_state->setErrorByName('twitter_handle', $error_message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $twitter_handle = $form_state->getValue('twitter_handle');

    $this->config($this->configName)
      ->set('twitter_handle', $twitter_handle)
      ->save();

    $messenger = \Drupal::messenger();

    $messenger->addMessage($this->t("The site's twitter handle has been set to %twitter_handle.", [
      '%twitter_handle' => $twitter_handle,
    ]));
  }

}
